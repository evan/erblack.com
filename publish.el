(require 'ox-publish)
(require 'htmlize)

(defun custom-sitemap-format-entry (entry style project)
  (format "[[file:%s][%s]] (%s)"
          entry
          (org-publish-find-title entry project)
          (format-time-string "%F" (org-publish-find-date entry project))))

(let ((user-full-name "Evan Black")
      (user-mail-address "evan@erblack.com")
      (org-export-with-toc nil)
      (org-export-with-section-numbers nil)
      (org-html-doctype "html5")
      (org-html-head "<link rel=\"stylesheet\" href=\"/style.css\">")
      (org-html-preamble nil)
      (org-html-postamble nil)
      (org-publish-project-alist
       '(("pages"
         :base-directory "org/"
         :base-extension "org"
         :recursive nil
         :publishing-directory "public/"
         :publishing-function org-html-publish-to-html)
        ("posts"
         :base-directory "org/posts/"
         :base-extension "org"
         :recursive t
         :publishing-directory "public/posts/"
         :publishing-function org-html-publish-to-html
         :auto-sitemap t
         :sitemap-title "Blog Posts"
         :sitemap-filename "index.org"
         :sitemap-style list
         :sitemap-sort-files anti-chronologically
         :sitemap-format-entry custom-sitemap-format-entry
         :with-title nil
         :html-html5-fancy t
         :html-metadata-timestamp-format "%Y-%m-%d"
         :html-preamble "<nav class=\"navigation\"><ul><li><a href=\"\/\">Home</a></li></ul></nav><h1 class=\"title\">%t</h1><time class=\"date\" datetime=\"%d\">%d</time>"
         :html-postamble "<nav class=\"navigation\"><ul><li><a href=\"\/\">Home</a></li></ul></nav>")
        ("assets"
         :base-directory "org/"
         :base-extension "css\\|js\\|txt\\|jpe?g\\|png\\|pdf"
         :include [".htaccess" "favicon.ico"]
         :recursive t
         :publishing-directory "public/"
         :publishing-function org-publish-attachment)
        ("erblack.com" :components ("pages" "posts" "assets")))))
  (org-publish "erblack.com"))

;; May need to run this for cleanup.
;; (org-publish-remove-all-timestamps)
;; (delete-directory "public" t)
