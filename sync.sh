#!/bin/sh

# Requires `erblack.com` in `~/.ssh/config`.
rsync -zvhr public/ erblack.com:/home/public/
